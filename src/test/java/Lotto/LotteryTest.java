package Lotto;

import org.junit.Test;

import static junit.framework.TestCase.*;
import static org.junit.Assert.assertArrayEquals;

public class LotteryTest {

    @Test
    public void getRandomNotZeroTest() throws IllegalInputException {
        Lottery lottery = new Lottery(1, 1);
        assertEquals(1, lottery.getRandomNo());
    }

    @Test
    public void getRandomBoundFunctionTest() throws IllegalInputException {
        Lottery lottery = new Lottery(1, 5);
        assertTrue(lottery.getRandomNo() < 6);

    }

    @Test
    public void lotteryParametersTest() throws IllegalInputException {
        Lottery lottery = new Lottery(6, 7);
        assertEquals(7, lottery.getBound());
        assertEquals(6, lottery.getTotalNumbersInOneLottery());

    }

    @Test
    public void lotteryArrayTest() throws IllegalInputException {
        Lottery lottery = new Lottery(6, 7);
        assertEquals(6, lottery.performLottery().length);
    }

    @Test(expected = IllegalInputException.class)
    public void invalidBoundInputTest() throws IllegalInputException {
        Lottery lottery = new Lottery(1, -1);
        lottery.performLottery();
    }


    @Test(expected = IllegalInputException.class)
    public void invalidArraySizeInputTest() throws IllegalInputException {
        Lottery lottery = new Lottery(-1, 1);
        lottery.performLottery();
    }

    @Test
    public void invalidInputTest() throws IllegalInputException {
        Lottery lottery = new Lottery(1, 1);
        lottery.performLottery();
        assertFalse(lottery.isSameNoInResult(2));
    }

    @Test
    public void sameNoInResultTest() throws IllegalInputException {
        Lottery lottery = new Lottery(1, 1);
        lottery.performLottery();
        assertTrue(lottery.isSameNoInResult(1));
    }

    @Test
    public void performLotteryTest() throws IllegalInputException {
        Lottery lottery = new Lottery(2, 2);
        int[] lotteryResult = lottery.performLottery();
        assertTrue(lotteryResult[0] != lotteryResult[1]);
    }

    @Test
    public void performLotteryTest2() throws IllegalInputException {
        Lottery lottery = new Lottery(6, 6);
        int[] lotteryResult = lottery.performLottery();
        int[] testLotteryResult = new int[]{1, 2, 3, 4, 5, 6};
        assertArrayEquals(lotteryResult, testLotteryResult);
    }
}
