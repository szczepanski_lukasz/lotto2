package Lotto;

import java.util.Arrays;
import java.util.Random;

public class Lottery {

    private int totalNumbersInOneLottery;
    private int bound;
    private int[] result;

    public Lottery(int totalNumbersInOneLottery, int bound) throws IllegalInputException {

        if (totalNumbersInOneLottery <= 0 || bound <= 0)
            throw new IllegalInputException();

        this.totalNumbersInOneLottery = totalNumbersInOneLottery;
        this.bound = bound;

        result = new int[totalNumbersInOneLottery];
    }

    private Random randomNo = new Random();

    public int getTotalNumbersInOneLottery() {
        return totalNumbersInOneLottery;
    }

    public int getBound() {
        return bound;
    }

    //losuje randomowy numer, max wartość w argumencie
    public int getRandomNo() {
        return randomNo.nextInt(this.bound) + 1;
    }

    public boolean isSameNoInResult(int noSearched) {
        for (int aResult : result) {
            if (aResult == noSearched) {
                return true;
            }
        }
        return false;
    }

    public int[] performLottery() {

        for (int i = 0; i < result.length; i++) {
            int randomNumber = getRandomNo();
            for (int j = 0; j < result.length; j++) {
                while (result[j] == randomNumber) {
                    randomNumber = getRandomNo();
                    j = 0;
                }
            }
            result[i] = randomNumber;
        }

        Arrays.sort(result);
        return result;
    }

    @Override
    public String toString() {
        return "Lottery{" +
                "result=" + Arrays.toString(result) +
                '}';
    }
}