package Lotto;

import java.util.ArrayList;

public class InternetLotteryResult {

    private ArrayList<Integer> resultFromInternet;

    private String date;

    public InternetLotteryResult(ArrayList<Integer> resultFromInternet, String date) {
        this.resultFromInternet = resultFromInternet;
        this.date = date;
    }

}
