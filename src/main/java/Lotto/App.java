package Lotto;

import java.util.Arrays;


public class App 
{
    public static void main( String[] args ) throws IllegalInputException {
        Lottery lottery = new Lottery(6,50);
        int counter = 0;
        int[] result;
        int [] los = new int[]{3,5,22,27,31,37};
        do {
            result = lottery.performLottery();
            counter++;
        } while (!Arrays.equals(los, result));
        System.out.println("Wydałeś " + counter * 3 + "PLN");
    }
}
